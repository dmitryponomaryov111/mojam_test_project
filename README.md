Requirements:
- python >= 3.8.2

For installing other requirements use following command:

`pip install -r requirements.txt`

In file `conftest.py` in fixtures `login` and `password` enter real steam account and password usually used for testing, also disable the steamguard for this account. 

To run the tests use following command:

` $ pytest `

To run the tests with generating .html report use following command:

` $ pytest --html=report.html `
