import pytest
from pytest import fixture
from selenium import webdriver
from chromedriver_py import binary_path


@fixture(scope='module')
def test_link():
    case_link = 'https://three.jcdev.ru/cases/open/falchion'
    return case_link


@fixture(scope='module')
def chrome_browser():
    browser = webdriver.Chrome(executable_path=binary_path)
    return browser


@fixture(scope='module')
def login():
    acc_name = ''
    return acc_name


@fixture(scope='module')
def password():
    valid_password = ''
    return valid_password

